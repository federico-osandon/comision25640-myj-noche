import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import ItemListContainer from './containers/ItemListContiner/ItemListContainer'
import NavBar from './components/NavBar/NavBar';
import Cart from './components/Cart/Cart';
import imagen from './img/remera.jpg'
import ItemDetailContainer from './containers/ItemDetailContainer/ItemDetailContainer';

import logo from './logo.svg'

import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'; 
import CartContextProvider from './context/cartContext';
           

function App() {
    

    return (
        
        
        <CartContextProvider>
            <BrowserRouter>
                    <div
                        //onClick={()=> aslert('soy evento de app')}
                        className='App border border-1 border-primary'           
                    >   
                        <NavBar />                
                        <Routes>
                            <Route path='/' element={<ItemListContainer greeting='soy ITEMLISTCONTAINER' />} />                                        
                            <Route path='/categoria/:categoriaId' element={<ItemListContainer greeting='soy ITEMLISTCONTAINER' />} />                                        
                            <Route path='/detalle/:detalleId' element={<ItemDetailContainer />} />
                            <Route path='/cart' element={<Cart />} />                    
                            <Route path='/*' element={ <Navigate to='/' replace /> } />
                        </Routes>
                    </div>
                </BrowserRouter>
        </CartContextProvider>
        
    )
}

export default App


























// condicion ? si  : no
    // condicion && si
    // condicion  || o esto
 //  let data = ['hola', 'fede', 'el mejor']
            //  data.forEach( (elem)=>{console.log(elem)}  )

            // let condition = true
            // let resultado = ''
            // if (condition) {
            //     resultado= 'verdader'
            // } else {
            //     resultado= 'false'                
            // }
            // console.log('el resultado '+ resultado);

            // let condition = true
            // console.log(`el resultado es: ${condition ? 'verdadero' : 'false'}`)


            // const array = ['b', 'c', 'd']
            
            // let a='a' 


            // const newArary = [a, ...array]// ['a',['b', 'c', 'd']]
            // console.log(newArary)

            // const id = '_fede'
            // const obj = { 
            //               foo: "bar", 
            //               [ "baz" + id ]: 42 
            //             }

            // console.log(obj);

            // var obj1 = { 
            //     a : 2,
            //     b: 5,
            //     c: 'juan'
            // }

            // let a = obj1.a
            // let b = obj1.b
            // const { a: val, b, c='fede' } = obj1

            
            //console.log(c);