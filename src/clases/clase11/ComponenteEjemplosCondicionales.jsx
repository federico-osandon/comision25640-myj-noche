import { useState, useEffect } from "react";


 
export const ControlledInput = () => {
    const [dataForm, setDataForm] = useState( {texto: '', nombre: '' } );

    useEffect(() => {
        console.log('addEventListener');        
        return () => console.log('removeEventListener')
    }, [dataForm])

    const handleOnChange = (e) =>{
        console.log(e.target.name)
        console.log(e.target.value) 
        setDataForm( {
            ...dataForm,
            [e.target.name] : e.target.value
         } )  

    }
    console.log(dataForm)
    return (
        <>
            <input
                type="text"
                name='texto'
                value={dataForm.texto}
                onChange={handleOnChange}
            />
            <input
                type="text"
                name='nombre'
                value={dataForm.nombre}
                onChange={handleOnChange}
            />
           
        </>
    );
  };


  const Loading = () => {
    // estados
    useEffect(() => {
        //acciones
        return ()=> console.log('desmontaod')
    })

    return <h2>Loading... </h2>
  }


  
export  function LoadingComponent() {
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {

        setTimeout(() => {
            setLoading(false);
        }, 5000);

        return ()=>{
            console.log('Limpiando componente');
        }
    }, []);
    
    return <>
        {loading ? <Loading /> : <h3>Productos cargardos!</h3>}
    </>;
  }











  
export  function TextComponent({ condition = false }) {
    
    if (!condition) {
        return <h2>Uds no esta logeado</h2>;//navigate
    }
  
    return (
        <>  
            <h2>Ud esta logueado puede ver la pág.</h2>
        </>
    )
  }








// condicion ? : ,  condicion &&  , condicion || 


export  function TextComponent2({ condition = false }) {
    // Llamado context 
    return (
        <>
            {condition && <h2>Ud esta logueado puede ver esta parte.</h2>}

            {!condition && <h2>Ud no esta logueado, NO puede ver esta sección.</h2>}

        </>
    );
  }

// condicion ? :(si no), condición && accion si, condicion  || acciones







export  function TextComponent3({ condition = true }) {
    return (
        <>
            <h2> {condition ? 'Ud esta logueado puede ver la pág.' : 'Ud esta logueado no puede ver la pág.'} </h2>            
            
        </>
    )
}









 export function TextComponent4({ condition = true }) {

    return (
        <>
            <h2 style={ { color: condition ? "green" : "red" } }>
                Ud esta logueado puede ver la pág.
            </h2>
        </>
    );
  }













  
export  function TextComponent5({ condition = true }) {
    return (
        <>
            <h2 className={ (condition === true) ? "btn btn-success" : "btn btn-danger" }>
            stock
            </h2>
        </>
    );
}

















export  function TextComponent6( { condition = true, otro='mt-5' }  ) {
    return (
        <>
            <h2
                className={ `${condition === true ? "btn btn-success" : "btn btn-danger"} ${otro || ""} `}
            >
            Ud esta logueado puede ver la pág.
            </h2>
        </>
    );
}












export function TextComponent7({ condition = false , otro = "mt-5" }) {
    
    const config = condition

        ?
            {
                className: `btn btn-success ${otro || ""}`,
                style: {color: 'red'},
                title: "Este es el titulo si la condicion es verdadera",
                nombre: 'Fede'
            }
        : 
            {
                className: `btn btn-warning ${otro || ""}`,
                style: {color: 'green'},
            }

            
    
      return (
        <>
            <h2 {...config} >Ud esta logueado puede ver la pág.</h2>
        </>
    )
  }