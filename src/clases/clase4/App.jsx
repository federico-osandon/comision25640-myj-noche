import { useState } from 'react'
import logo from './logo.svg'
import './App.css'
import TituloApp from './components/Titulo/TituloApp'
import ComponenteClase from './components/ComponenteClase'
import InputApp from './components/Input/Input'
import ComponenteContenedor from './components/ComponenteContedor/ComponenteContenedor'
import ComponenteImagen from './components/IconoCart'

  
           

function App() { // componente contendor
    const [count, setCount] = useState(0)

    const style = { backgroundColor: 'blue'} // estado
    const tit = 'Este titulo viene de app' // estado
    const saludo = () => console.log('saludo de app')

    // propiedades son obj de js, pero a la vez 
    // TituloApp({titulo, subtitulo})
    return (
        <div
            className='App'
            style={ style }
            onClick={ () => console.log('soy evento de app') }
        >   
            {/* nav */}
            <TituloApp  titulo= {tit} subtitulo='soy subtitulo' saludo={saludo} />
            <InputApp >
                <ComponenteImagen />            
                <ComponenteImagen />            
            </InputApp> 
            <ComponenteContenedor saludo='hola soy componente contenedor' />
                 
        </div>
    )
    }

export default App


























// condicion ? si  : no
    // condicion && si
    // condicion  || o esto
 //  let data = ['hola', 'fede', 'el mejor']
            //  data.forEach( (elem)=>{console.log(elem)}  )

            // let condition = true
            // let resultado = ''
            // if (condition) {
            //     resultado= 'verdader'
            // } else {
            //     resultado= 'false'                
            // }
            // console.log('el resultado '+ resultado);

            // let condition = true
            // console.log(`el resultado es: ${condition ? 'verdadero' : 'false'}`)


            // const array = ['b', 'c', 'd']
            
            // let a='a' 


            // const newArary = [a, ...array]// ['a',['b', 'c', 'd']]
            // console.log(newArary)

            // const id = '_fede'
            // const obj = { 
            //               foo: "bar", 
            //               [ "baz" + id ]: 42 
            //             }

            // console.log(obj);

            // var obj1 = { 
            //     a : 2,
            //     b: 5,
            //     c: 'juan'
            // }

            // let a = obj1.a
            // let b = obj1.b
            // const { a: val, b, c='fede' } = obj1

            
            //console.log(c);