import { useState } from "react"
import { 
  addDoc, 
  collection, 
  doc, 
  documentId, 
  getDocs, 
  getFirestore, 
  query, 
  updateDoc, 
  where, 
  writeBatch 
} from "firebase/firestore"
import { useCartContext } from "../../context/cartContext"


function Cart() {
    const [dataForm, setDataForm] = useState({
      email: '', name: '', phone: ''
    })
    const [id, setId] = useState('')

    const { cartList, vaciarCart, precioTotal, removeItem } = useCartContext()

    // fucntion {}
    const generarOrden= async (e)=>{
      e.preventDefault();
      // Nuevo objeto de orders    
      let orden = {}      

      orden.buyer = dataForm
      orden.total = precioTotal();

      orden.items = cartList.map(cartItem => {
          const id = cartItem.id;
          const nombre = cartItem.name;
          const precio = cartItem.price * cartItem.cantidad;
          
          return {id, nombre, precio}   
      })
      console.log(orden)

      // crear

      const db = getFirestore()
      const queryCollectionSet = collection(db, 'orders')
      addDoc(queryCollectionSet, orden)
      .then(resp => setId(resp.id))
      .catch(err => console.error(err))
      .finally(() => console.log('termino '))

      //update 
      // const queryUpdate =  doc(db, 'items', 'YqfRcxZcHXPiQ6Tz47D7')
      // updateDoc(queryUpdate, {
      //   stock: 99
      // })


      // OPCIONAL

      // const queryCollection = collection(db, 'items')

      // const queryActulizarStock = await query(
      //   queryCollection, //                   ['jlksjfdgl','asljdfks'],  
      //   where( documentId() , 'in', cartList.map(it => it.id) )          
      // )

      // const batch = writeBatch(db)

      // await getDocs(queryActulizarStock)
      //   .then(resp => resp.docs.forEach(res => batch.update(res.ref, {
      //       stock: res.data().stock - cartList.find(item => item.id === res.id).cantidad
      // }) ))

      // batch.commit()
    }


    const handleChange = (e) => {
        setDataForm({
          ...dataForm,
          [e.target.name]: e.target.value
      })
    }


    console.log(dataForm)
    return (
      <div>
        {id.length !== '' && `el id de la compra es: ${id}`}
        { cartList.map(item =>  <>
                                  <li key={item.id}>
                                    nombre: {item.name} precio: {item.price} cantidad:{item.cantidad}
                                  </li>
                                  <button onClick={() => removeItem(item.id) }> X </button> <hr></hr>
                                </>
                      )
                                
        }
        <button onClick={vaciarCart}>VaciarCarrto</button>
        <form 
                onSubmit={generarOrden}                 
            >
                <input 
                    type='text' 
                    name='name' 
                    placeholder='name' 
                    value={dataForm.name}
                    onChange={handleChange}
                /><br />
                <input 
                    type='text' 
                    name='phone'
                    placeholder='tel' 
                    value={dataForm.phone}
                    onChange={handleChange}
                /><br/>
                <input 
                    type='email' 
                    name='email'
                    placeholder='email' 
                    value={dataForm.email}
                    onChange={handleChange}
                /><br/>
                {/* <button>Generar Orden</button> */}
                <button>Terminar Compra</button>
            </form>
      </div>
    )
}

export default Cart