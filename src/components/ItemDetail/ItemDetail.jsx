import { useState } from "react"
import Caso1 from "../../clases/clase9/Caso1"
import Caso2 from "../../clases/clase9/Caso2"
import { Input } from "../../clases/clase9/Input"
import Intercambiabilidad from "../../clases/clase9/Intercambiabilidad"
import ItemCount from "../ItemCount/ItemCount"
import imag from '../../img/remera.jpg'
import { Link } from "react-router-dom"
import { useCartContext } from "../../context/cartContext"
// import { useCartContext } from "../../context/cartContext"


const ItemDetail = ({producto}) => {
  const [count, setCount] = useState(null)

  const { agregarCart, cartList } = useCartContext()

  const onAdd = cant =>{
    console.log(cant)
    setCount(cant)
    agregarCart({ ...producto, cantidad: cant })
  }
  console.log(cartList)

  return (
    <div>
        <h2>{producto.name}</h2>
        <img src={producto.foto} alt='imágen' className='w-25' />
        {producto.name}
        { count ?
            <Link to='/cart'>
              <button className='btn btn-outline-primary'>Ir al Cart</button>
            </Link> 
          :
            <ItemCount initial={1} stock={10} onAdd={onAdd} />
        }
    </div>
  )
}

export default ItemDetail

