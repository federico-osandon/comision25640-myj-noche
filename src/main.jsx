import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import getFirestoreApp from './firebase/config'
// import AppEcommerce from './clases/clase11/AppEcommerce'

getFirestoreApp()

 //     App()
ReactDOM.render(     
    <App />,
  document.getElementById('root')
)
