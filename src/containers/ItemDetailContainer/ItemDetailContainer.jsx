import { doc, getDoc, getFirestore } from 'firebase/firestore'
import {useEffect, useState} from 'react'
import { useParams } from 'react-router-dom'
import ItemDetail from '../../components/ItemDetail/ItemDetail'
import getFetch, { getFetchProd } from '../../helpers/getFetch'

const ItemDetailContainer = () => {
    const [producto, setProducto] = useState({})
    const [loading, setLoading] = useState(true)

    const { detalleId } = useParams()
    
    console.log(detalleId)
    // llamada a api
    
    useEffect(() => {
        const db = getFirestore()
        const queryDb = doc(db, 'items', detalleId)
        getDoc(queryDb)
        .then(resp => setProducto( { id: resp.id, ...resp.data() } ))
        .catch(err => console.log(err))
        .finally(() =>setLoading(false))
        // .then(resp => console.log(resp))
    },[])

    // useEffect (() => {
    //     getFetchProd
    //     .then(resp => console.log(resp))
    // }, [])


    //console.log(producto)
  return (
    <>
        {loading ?

          <h2>Cargando...</h2> 
          :        
          <ItemDetail producto={producto} />
        }
    </>
  )
}

export default ItemDetailContainer